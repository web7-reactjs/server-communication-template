import logo from "./logo.svg";
import "./App.css";
import MyNavBar from "./components/MyNavBar";
import "bootstrap/dist/css/bootstrap.min.css";
import MyCard from "./components/MyCard";
import ProductCard from "./components/ProductCard";
import NotFoundPage from "./pages/NotFoundPage";
import InsertProduct from "./pages/InsertProduct";
import InsertUser from "./pages/InsertUser";
import ViewProfile from "./pages/ViewProfile";
function App() {
  return (
    <div>
      <MyNavBar />
      <ViewProfile/>
      <InsertUser/>
      <InsertProduct/>
      <NotFoundPage/>
      <MyCard />
      <ProductCard/>
      <h1> here is header for the app js </h1>
    </div>
  );
}

export default App;
