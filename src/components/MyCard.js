import React from "react";

import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
const MyCard = () => {
  return (
    <>
      <Card style={{ width: "18rem" }}>
        <Card.Img variant="top" src="https://i.pinimg.com/736x/62/ab/dd/62abdd395246ad51481c162b985a61a0.jpg" />
        <Card.Body>
          <Card.Title>Card Title <span className="text-warning"> Admin </span> </Card.Title>
          <Card.Text>
            <p> webdev@gmail.com</p>
            Some quick example text to build on the card title and make up the
            bulk of the card's content.
          </Card.Text>
          <div className="d-flex justify-content-between">
            <Button variant="primary">View</Button>
            <Button variant="warning">Update</Button>
          </div>
        </Card.Body>
      </Card>
    </>
  );
};

export default MyCard;
