import React from 'react'

const InsertUser = () => {
  return (
     <>
      <div className="container ">
        <div className="d-flex mt-5 rounded-5  bg-light  justify-content-center align-items-center gap-4">
          <div className="image w-50">
            <img
              className="object-fit-contain  w-100" alt=""
              src="https://cdn3d.iconscout.com/3d/premium/thumb/man-holding-sign-up-form-2937684-2426382.png"
            />
          </div>
          <div className="form-side  ">
              <div className="   mt-4 rounded-5 pt-5 px-3 pb-2">
                <h1> Create New Users </h1>

                <div className="my-5  ">
                  <form action="">
                    <label htmlFor="inputName"> Username </label>
                    <input
                     
                      id="inputName"
                      type="text"
                      name="inputName"
                      placeholder="Enter username "
                      className="form-control"
                    />
                    <label htmlFor="inputEmail"> Email</label>
                    <input
                      
                      id="inputEmail"
                      name="inputEmail"
                      type="text"
                      placeholder="Enter email "
                      className="form-control"
                    />
                    <label htmlFor="inputPassword"> Password </label>
                    <input
                       
                      type="password"
                      id="inputPassword"
                      name="inputPassword"
                      placeholder="Enter password "
                      className="form-control"
                    />
                    <label htmlFor="inputAvatar"> Avatar </label>
                    <input
                     
                      type="text"
                      id="inputAvatar"
                      name="inputAvatar"
                      placeholder="Enter Avatar Links "
                      className="form-control"
                    />

                    <button
                     
                      className="
                btn
                 btn-warning 
                 my-2"
                     
                    >
                      Create Users
                    </button>

               
                  </form>
                </div>
              </div>
             
             
          </div>
        </div>
      </div>
     </>
  )
}

export default InsertUser